import pandas as pd
from src.etl.ETL import ETL
from pandas.tseries.offsets import MonthEnd
from src.data_prep.Preprocessing import Preprocessing
from src.prediction.PredictionPipeline import PredictionPipeline

model_names = ['bariatrica', 'neonatal', 'ortopedia', 'bucomaxilo', 'cardio']


def get_last_day_of_month(str_date):
    date = pd.to_datetime(str_date) + MonthEnd(1)
    return str(date.date())


def get_prediction_parameters(request_json):
    params = request_json.copy()

    try:
        model_name = params['model']
        if model_name not in model_names:
            raise ValueError(f'Incorrect model name. Try one of these: {model_names}.')
    except KeyError:
        raise KeyError(f'Call to this function must include model name. Try one of these: {model_names}.')

    try:
        ref_date = params['ref_date']
        params['ref_date'] = get_last_day_of_month(ref_date)
    except KeyError:
        raise KeyError(f'Call to this function must include reference date. Try this format: "YYYY-mm-dd".')

    try:
        params['id_empresa']
    except KeyError:
        raise KeyError(f'Call to this function must include id_empresa.')

    return params


def run_etl(model_name, ref_date, id_empresa):
    print('Iniciando ETL dos dados de predict.')
    etl = ETL(model_name, ref_date, id_empresa)
    etl_data = etl.run()
    
    return etl_data


def run_preprocessing(df):
    print('Iniciando pré-processamento dos dados de predict.')
    prep = Preprocessing()
    preprocessed_df = prep.run(df)
    return preprocessed_df


def run_predict(df, model_name, id_empresa, ref_date):
    print('Iniciando pipeline de predict do modelo.')
    tp = PredictionPipeline(model_name, id_empresa, ref_date)
    tp.run(df)


def run(request_json):
    """Point of access for the prediction pipeline.

    Args:
        request_json (dict): Dictionary object which contains the prediction parameters such as:
            model, id_empresa and ref_date.
    """

    # Get prediction options from request_json data.
    print(f'These are the prediction pipeline parameters: {request_json}.')
    params = get_prediction_parameters(request_json)
    model_name, ref_date, id_empresa = params['model'], params['ref_date'], params['id_empresa']

    print(f'Start prediction procedure.')
    etl_data = run_etl(model_name, ref_date, id_empresa)
    
    preprocessed_df = run_preprocessing(etl_data)
    
    run_predict(preprocessed_df, model_name, id_empresa, ref_date)
